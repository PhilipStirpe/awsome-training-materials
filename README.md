# README #

AWSome-training-materials


I produced this set of files to help me prepare for and deliver the demos at AWSome Glasgow 2015

I used the script supplied (demo-script-v2.txt) as a starting point but then developed a step by step guide in Word.

It wasn't intended that I would follow this guide during the demo (unless I had two monitors). 
More likely, it was to help me clarify the steps and act as a guide to other trainers wanting to prepare to deliver the demos.

In order to reduce the setup steps (and therefore stress), I tried to put as much of it into a Cloudformation template.

In fact, the setup now involves the creation of a keypair and creation of a stack.

The added bonus of using Cloudformation is that I was able to show the template to the audience for an additional demo at the end of the day and then have them watch while I deleted the stack.

There are a couple of things I would like to change in future. There was a requirement that the default vpc had three subnets. 
In fact, I didn't have any in my account that fitted the bill but I was pretty sure that I could manage with two based upon my understanding of the demos.
As most of my other regions didn't have default VPCs, I chose to use us-west-1.

In fact, I had to hard code some of the template accordingly.

I tried to make the CFN template place the RDS instance into a security group which i failed at.

I managed to do it with the two Windows servers but not the RDS. I recall that the property for the RDS security group was different.

If we could make that work, then (as far as I understand it) we will not be reliant on the default vpc?

I have a mapping section for the Windows AMIs. Actually, only the us-west-1 ami is correct. The others were copied from an aws document and relate to Windows 2008?

I would like to know what the mimimum instance types could be for the Windows servers and DB instance. I used m1.large and m3.large.

If anyone would like to contribute to this project, I would be most grateful.

Please get in touch if you have any questions/suggestions.



Philip Stirpe
Head of Agile Software Development
QA Training Limited

phil.stirpe@qa.com


### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact